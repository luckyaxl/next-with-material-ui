import React, { createContext, useState } from 'react';

const UserContext = createContext();

export default function useUser() {
  return React.useContext(UserContext);
}

export function UserProvider(props) {
  // state
  const [user, setUser] = useState(null);
  const [confirm, setConfirm] = useState({ onYes: null, value: false });

  const { children } = props;
  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
        confirm,
        setConfirm,
      }}>
      {children}
    </UserContext.Provider>
  );
}
