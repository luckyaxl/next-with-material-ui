import { createMuiTheme } from '@material-ui/core/styles';
import Colors from './colors';

// const ProductSans = {
//   fontFamily: 'ProductSans',
//   fontStyle: 'normal',
//   fontDisplay: 'swap',
//   fontWeight: 'normal',
//   src: `url('/fonts/ProductSans-Regular.woff') format("truetype")`,
// };

// const ProductSansMedium = {
//   fontFamily: 'ProductSans',
//   fontStyle: 'normal',
//   fontDisplay: 'swap',
//   fontWeight: 500,
//   src: `url('/fonts/ProductSans-Medium.woff') format("truetype")`,
// };

// const ProductSansItalic = {
//   fontFamily: 'ProductSans',
//   fontStyle: 'italic',
//   fontDisplay: 'swap',
//   fontWeight: 500,
//   src: `url('/fonts/ProductSans-Italic.woff') format("truetype")`,
// };

// const ProductSansBold = {
//   fontFamily: 'ProductSans',
//   fontStyle: 'normal',
//   fontDisplay: 'swap',
//   fontWeight: 'bold',
//   src: `url('/fonts/ProductSans-Medium.woff') format("truetype")`,
// };

const theme = createMuiTheme({
  props: {
    MuiButton: {
      disableRipple: true,
      disableElevation: true,
    },
    MuiMenuItem: {
      disableRipple: true,
    },
  },
  palette: {
    background: {
      default: Colors.LIGHT_GREY,
    },
    common: {
      black: '#050505',
    },
    primary: {
      main: Colors.PRIMARY,
    },
    success: {
      main: `${Colors.SECONDARY} !important`,
    },
  },
  typography: {
    body2: {
      //fontWeight: 600,
      fontSize: 14,
    },
    button: {
      textTransform: 'none',
      //fontWeight: 500,
    },
    fontFamily: `-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif`,
  },
  overrides: {
    MuiCard: {
      root: {
        boxShadow: '0px 0px 4px rgba(5, 5, 5, 0.08)',
      },
    },
    MuiCardContent: {
      root: {
        padding: 12,
      },
    },
    MuiButton: {
      root: {
        fontWeight: 500,
        borderRadius: 4,
      },
    },
    MuiTableCell: {
      root: {
        borderBottom: 'none',
      },
    },
    MuiTableBody: {
      root: {
        background: '#FFF',
      },
    },
    MuiContainer: {
      root: {
        marginLeft: 0,
        marginRight: 0,
      },
    },
    MuiListItem: {
      root: {
        // '&:hover': {
        //   backgroundColor: 'rgba(10, 255, 255, 0.1) !important',
        // },
        '&.Mui-selected': {
          background: '#6B47FF',
          color: '#FFF',
          '&:hover': {
            background: '#05367f !important',
          },
        },
      },
    },
    // MuiCssBaseline: {
    //   '@global': {
    //     '@font-face': [
    //       ProductSans,
    //       ProductSansMedium,
    //       ProductSansItalic,
    //       ProductSansBold,
    //     ],
    //   },
    // },
    MuiTextField: {
      root: {
        fontSize: '12px !important',
      },
    },
  },
});

export default theme;
