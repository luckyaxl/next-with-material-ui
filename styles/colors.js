const Colors = {
  BLACK: '#454245',
  DARK_GREY: '#7A91B2',
  PRIMARY: '#6B47FF',
  SECONDARY: '#13C3B8',
  WHITE: '#FFFFFF',
  LIGHT_GREY: '#F6F6F7',
  RED: '#F82B60',
  YELLOW: '#FBC02D',
};
export default Colors;
